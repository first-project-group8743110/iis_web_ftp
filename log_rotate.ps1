$Path = "C:\Logs"
$BackupFolder = "C:\FTP\Backup"
$DaysToBackup = 1
$DaysToDel = 3

#---Backuping log files for $DaysToBackup---
$Files = Get-ChildItem -Path $Path -File | ForEach-Object { Get-ChildItem $_.FullName | Where-Object { $_.LastWriteTime -gt (Get-Date).AddDays(-$DaysToBackup) } }

$ZipName = ("{0}\{1}_Backup.zip" -f $BackupFolder, (Get-Date -f yyyyMMdd_HHmmss))

Compress-Archive -Path $Files.FullName -DestinationPath $ZipName

#---Clear log files older than $DaysToDel---
Get-ChildItem -Path $Path -File | Where-Object {($_.LastWriteTime -lt (Get-Date).AddDays(-$DaysToDel))} | Remove-Item -Force