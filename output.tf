output "vm-name" {
  value = google_compute_instance.iis_instance.name
}

output "external-ip" {
  value = google_compute_instance.iis_instance.network_interface.0.access_config.0.nat_ip
}

output "internal-ip" {
  value = google_compute_instance.iis_instance.network_interface.0.network_ip
}