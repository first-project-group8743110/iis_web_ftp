terraform {
  required_version = "~> 1.0"

  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
}

provider "google" {
  project     = var.gcp_project
  region      = var.gcp_region
  zone        = var.gcp_zone
}

# create VPC
resource "google_compute_network" "vpc" {
  name                    = "${lower(var.server_name)}-vpc"
  auto_create_subnetworks = "false"
  routing_mode            = "GLOBAL"
}

# create public subnet
resource "google_compute_subnetwork" "network_subnet" {
  name          = "${lower(var.server_name)}-subnet"
  ip_cidr_range = var.network_subnet_cidr
  network       = google_compute_network.vpc.name
  region        = var.gcp_region
}

# allow winrm
resource "google_compute_firewall" "allow_winrm" {
  name    = "${var.server_name}-winrm"
  network = google_compute_network.vpc.name
  allow {
    protocol = "tcp"
    ports    = ["5985", "5986"]
  }
  
  source_ranges = ["0.0.0.0/0"]
  target_tags = ["winrm"] 
}

# allow http
resource "google_compute_firewall" "allow_http" {
  name    = "${var.server_name}-http"
  network = google_compute_network.vpc.name
  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  
  source_ranges = ["0.0.0.0/0"]
  target_tags = ["http"] 
}

# allow https
resource "google_compute_firewall" "allow_https" {
  name    = "${var.server_name}-https"
  network = google_compute_network.vpc.name
  allow {
    protocol = "tcp"
    ports    = ["443"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["https"] 
}

# allow ftp
resource "google_compute_firewall" "allow_ftp" {
  name    = "${var.server_name}-ftp"
  network = google_compute_network.vpc.name
  allow {
    protocol = "tcp"
    ports    = ["21", "45500-45600"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["ftp"]
}

# allow rdp
resource "google_compute_firewall" "allow_rdp" {
  name    = "${var.server_name}-rdp"
  network = google_compute_network.vpc.name
  allow {
    protocol = "tcp"
    ports    = ["3389"]
  }

  source_ranges = ["91.200.156.0/22","31.135.151.223"]
  target_tags = ["rdp"]
}

# Create instance
resource "google_compute_instance" "iis_instance" {
  name         = var.server_name
  machine_type = var.windows_instance_type
  zone         = var.gcp_zone
  tags         = ["rdp","http","https","ftp","winrm"]

  boot_disk {
    auto_delete = true
    initialize_params {
      image = var.windows_image_version
      size = var.windows_disk_size
    }
  }

  network_interface {
    network       = google_compute_network.vpc.name
    subnetwork    = google_compute_subnetwork.network_subnet.name
    access_config { }
  }

  depends_on = [
    google_compute_network.vpc,google_compute_subnetwork.network_subnet
  ] 
}

resource "time_sleep" "wait_x_min" {
  depends_on = [google_compute_instance.iis_instance]

  create_duration = "4m"
}

resource "null_resource" "get_new_admin_pass" {
  provisioner "local-exec" {
    command = "gcloud compute reset-windows-password iis --quiet --zone='${var.gcp_zone}'  | grep password | cut -d ' ' -f2- | tr -d ' ' | tr -d '\n' > pass.txt"
  }

  depends_on = [
    google_compute_instance.iis_instance, time_sleep.wait_x_min
  ]
}

