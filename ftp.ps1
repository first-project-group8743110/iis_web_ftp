#Creating new FTP site
$SiteName = "FTP"
$RootFolderpath = "C:\FTP"
$PortNumber = 21
$FTPUserName = "FtpUser"
$FTPPassword = Get-Content -Path "C:\Scripts\pass_ftp.txt" | Out-String

New-WebFtpSite -Name $SiteName -PhysicalPath $RootFolderpath -Port $PortNumber -Verbose -Force 


# Creating an FTP user
If (!(Get-LocalUser $FTPUserName -ErrorAction SilentlyContinue)) {
    New-LocalUser -Name $FTPUserName -Password $FTPPassword `
        -Description "User account to access FTP server" `
        -UserMayNotChangePassword
} 

# Enabling basic authentication on the FTP site
$param = @{
    Path    = 'IIS:\Sites\FTP'
    Name    = 'ftpserver.security.authentication.basicauthentication.enabled'
    Value   = $true 
    Verbose = $True
}
Set-ItemProperty @param

# Adding authorization rule to allow FTP users 
$param = @{
    PSPath   = 'IIS:\'
    Location = $SiteName 
    Filter   = '/system.ftpserver/security/authorization'
    Value    = @{ accesstype = 'Allow'; users = $FTPUserName; permissions = 3 } 
}

Add-WebConfiguration @param

# Changing SSL policy of the FTP site
'ftpServer.security.ssl.controlChannelPolicy', 'ftpServer.security.ssl.dataChannelPolicy', 'ftpServer.logFile.enabled' | 
ForEach-Object {
    Set-ItemProperty -Path "IIS:\Sites\FTP" -Name $_ -Value $false
}

$ACLObject = Get-Acl -Path $RootFolderpath
$ACLObject.SetAccessRule(
    ( # Access rule object
        New-Object System.Security.AccessControl.FileSystemAccessRule(
            $FTPUserName,
            'ReadAndExecute',
            'ContainerInherit,ObjectInherit',
            'None',
            'Allow'
        )
    )
)
Set-Acl -Path $RootFolderpath -AclObject $ACLObject

# Set pasive FTP port range and external IP
$publicIP = Invoke-RestMethod http://ipinfo.io/json | Select-Object -exp ip
Set-ItemProperty -Path "IIS:\Sites\$SiteName" -Name ftpServer.firewallSupport.externalIp4Address -Value $publicIP
Set-WebConfiguration "/system.ftpServer/firewallSupport" -PSPath "IIS:\" -Value @{lowDataChannelPort="45500";highDataChannelPort="45600";}

# Delete FtpUser pass file
Remove-Item "C:\Scripts\pass_ftp.txt"