#!/bin/bash

user_name=$(whoami)
password=$(cat ./pass.txt)
external_ip=$(terraform output -json vm_external_ip | tr -d '"')

# Generate host file for ansible
cat << EOF > ./host.yml
iis:
  hosts:
    first:
      ansible_port: 5985
      ansible_host: $external_ip
      ansible_user: $user_name
      ansible_password: $password
      ansible_connection: winrm
      ansible_winrm_scheme: http
      ansible_winrm_transport: basic
EOF

#Install to cloud shell ansible
sudo apt install ansible
pip install --upgrade ansible

# Trigger ansible execution
ansible-playbook -i ./host.yml -e "admpass=$password admuser=$user_name"  main.yml -vvv
