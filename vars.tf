variable "gcp_project" {
  type        = string
  description = "GCP project name"
  default = "cogent-tide-383507"
}

variable "gcp_region" {
  type        = string
  description = "GCP region"
  default = "europe-central2"
}

variable "gcp_zone" {
  type        = string
  description = "GCP zone"
  default = "europe-central2-a"
}

variable "server_name" {
  type        = string
  description = "This variable defines the server name used to build resources"
  default = "iis"
}

variable "network_subnet_cidr" {
  type        = string
  description = "The CIDR for the network subnet"
  default = "10.0.1.0/24"
}

variable "windows_image_version" {
  type        = string
  description = "SKU for Windows Server"
  default     = "windows-cloud/windows-2022"
}

variable "windows_instance_type" {
  type        = string
  description = "VM instance type"
  default     = "e2-medium"
}

variable "windows_disk_size" {
  type        = string
  description = "VM disk size"
  default     = "50"
}

